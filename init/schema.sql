DROP TABLES IF EXISTS `modulelog`;

CREATE TABLE `modulelog` (
    `id`	SERIAL PRIMARY KEY,
    `module`	TEXT NOT NULL,
    `version`	TEXT NOT NULL,
    `operation`	TEXT NOT NULL,
    `user`	TEXT,
    `host`	TEXT,
    `internal`	BOOLEAN NOT NULL DEFAULT FALSE,
    `caller`	TEXT NOT NULL,
    `utctime`	TIMESTAMP NOT NULL,
    `logged_utctime`	TIMESTAMP NOT NULL,
    `remark`	TEXT NOT NULL
);

